
vim-filemark is a helper plugin to generate filemark urls in Vim.

For documentation see (:h vim-filemark).

To run tests use (these require the vader plugin, so adjust NORC as needed):

	nvim -u NORC --cmd "let &rtp.=',.'" -c 'Vader! tests/tests.vader'

TODO:

- allow for local reviews (per tab/window?)
