" This file is part of vim-filemark.
"
" vim-filemark is free software: you can redistribute it and/or modify it under the
" terms of the GNU General Public License as published by the Free Software
" Foundation, either version 3 of the License, or (at your option) any later
" version.
"
" filemark is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
" A PARTICULAR PURPOSE. See the GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License along with
" filemark. If not, see <https://www.gnu.org/licenses/>.


if exists('g:loaded_vim_filemark')
    finish
endif
let g:loaded_vim_filemark = 1

function! s:hash_md5sum(filepath) abort
	if has('nvim') == 0
		let args = 'md5sum ' .. a:filepath
	else
		let args = ['md5sum', a:filepath]
	endif

	let hash = split(system(args))[0]
	if v:shell_error
		throw 'Failed to run md5sum to generate hash'
	endif
	return '&md5=' .. hash
endfun

function! s:hash_openssl(filepath) abort
	if has('nvim') == 0
		let args = 'openssl md5 -r ' .. a:filepath
	else
		let args = ['openssl', 'md5', '-r', a:filepath]
	endif

	let hash = split(system(args))[0]
	if v:shell_error
		throw 'Failed to run openssl to generate hash'
	endif
	return '&md5=' .. hash
endfunction "}}}

function! s:hash_none(filepath) abort
	return ''
endfun

" Determine hash function
if executable('md5sum')
	let s:hash_function = function('s:hash_md5sum')
elseif executable('openssl')
	let s:hash_function = function('s:hash_openssl')
else
	let s:hash_function = function('s:hash_none')
end

function! s:char_needs_encoding(character) abort
    let ascii = char2nr(a:character)
    if ascii >= 48 && ascii <= 57
        return 0 " digits
    elseif ascii >= 65 && ascii <= 90
        return 0 " letters
    elseif ascii >= 97 && ascii <= 122
        return 0 " letters - uppercase
    elseif a:character == "-" || a:character == "_" || a:character == "." || a:character == "~"
        return 0
    endif
    return 1
endfunction

function! s:urlencode(string) abort
    let result = ""
    let chars = split(a:string, '.\zs')
    for c in chars
        if c == " "
            let result = result . "%20"
        elseif s:char_needs_encoding(c)
            let i = 0
            while i < strlen(c)
                let byte = strpart(c, i, 1)
                let decimal = char2nr(byte)
                let result = result . "%" . printf("%02x", decimal)
                let i += 1
            endwhile
        else
            let result = result . c
        endif
    endfor

    return result

endfunction

" global state
let s:review = {}

function! s:check_review() abort
	if s:review == {}
		throw "No review is currently in progress, call FilemarkBegin"
	end
endfun

function! s:review_relpath(path) abort
    let cwd = getcwd()
    call chdir(FilemarkWorkdir())
    let r = fnamemodify(a:path, ":.")
    call chdir(cwd)
    return r
endfunction

function! s:get_md5hash(filepath) abort
	let F = get(g:, 'FilemarkHashFunction', s:hash_function)
	return F(a:filepath)
endfun

" Build filemark url
function! s:filemark_url(filepath, link, startline, endline, description) abort
	let url = 'filemark:?'

	let filepath = fnamemodify(a:filepath, ':p')
	let u_hash = s:get_md5hash(filepath)
	let u_file = s:review_relpath(filepath)	
	let u_file = 'file=/' .. u_file

	if a:startline == a:endline
		let u_lines = '&lines=' .. a:startline
	else
		let u_lines = '&lines=' .. a:startline .. '-' .. a:endline
	end

	let u_link = ''
	if a:link
		let u_link = '&link'
	end

	let u_description = ''
	if len(a:description) > 0
		let enc_desc = s:urlencode(a:description)
		echom enc_desc
		let u_description = '&description=' ..  enc_desc
	end

	let url = url .. u_file .. u_hash .. u_lines .. u_link .. u_description
	return url
endfun

function! s:default_url_handler(url)
	echo a:url
endfun

function! s:url_ready(url) abort
	call get(g:, 'FilemarkCallback', function('s:default_url_handler'))(a:url)
endfun

"
" These functions map to Commands
"
function! s:FilemarkBegin(...)
	try
		if a:0 < 1
			let root_dir = fnamemodify('.', ':p')
		else
			let root_dir = fnamemodify(a:1, ':p')
		end
		let s:review = {'root_path': root_dir }
	catch
		echoerr v:exception
	endtry
endfun

function! s:FilemarkFinish()
	let s:review = {}
endfun

function! s:FilemarkInfo()
	if s:review == {}
		echom "No review is in progress"
	else
		let root_path = FilemarkWorkdir()
		echo "Review root path: " .. root_path
	endif
endfun

function! s:FilemarkSelection(startline, endline, ...)
	try
		call s:check_review()

		let description = join(a:000, ' ')

		let filepath = bufname()
		if filepath == ''
			throw "The current buffer is not saved to disk"
		end
		" FIXME throw on modified buffer

		let url = s:filemark_url(filepath, 0, a:startline, a:endline, description)
		call s:url_ready(url)
	catch
		echoerr v:exception
	endtry
endfun

function! s:FilemarkLink(...) range
	try
		call s:check_review()

		let description = join(a:000, ' ')

		let filepath = bufname()
		if filepath == ''
			throw "The current buffer is not saved to disk"
		end

		let url = s:filemark_url(filepath, 1, 1, 1, description)
		call s:url_ready(url)
	catch
		echoerr v:exception
	endtry
endfun

function! s:FilemarkGoto()
	try
		call s:check_review()
		let root_path = FilemarkWorkdir()

		let line = getline('.')
		let start = stridx(line, 'filemark:')
		if start == -1
			echoerr 'Could not find filemark in current line'
			return
		endif

		let path = matchstr(line, '.*file=\zs.\{-}\ze\(&\|$\)', start)
		if path == ''
			echoerr 'Could not find filemark file in current line'
			return
		endif

		let targetline = 1
		let lines = matchstr(line, '.*lines=\zs.\{-}\ze\(&\|$\)', start)
		if lines != ''
			let targetline = str2nr(lines)
		endif

		let abspath = root_path .. '/' .. path
		execute 'e +' .. targetline .. ' ' .. abspath
	catch
		echoerr v:exception
	endtry
endfun

" Public functions
function! FilemarkUrl(filepath, link, startline, endline, description) abort
	try
		call s:check_review()
		return s:filemark_url(a:filepath, a:link, a:startline, a:endline, a:description)
	catch
		echoerr v:exception
	endtry
endfun

function! FilemarkWorkdir() abort
	return get(s:review, 'root_path', '')
endfun

" Commands
command! -nargs=? -complete=dir FilemarkBegin call s:FilemarkBegin(<f-args>)
command! -nargs=0 FilemarkFinish call s:FilemarkFinish()
command! -nargs=0 FilemarkGoto call s:FilemarkGoto()
command! -nargs=0 FilemarkInfo call s:FilemarkInfo()
command! -nargs=* -range FilemarkSelection call s:FilemarkSelection(<line1>,<line2>,<f-args>)
command! -nargs=* FilemarkLink call s:FilemarkLink(<f-args>)

